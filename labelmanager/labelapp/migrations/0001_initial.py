# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('x', models.CharField(max_length=60, null=True, blank=True)),
                ('y', models.CharField(max_length=60, null=True, blank=True)),
                ('w', models.CharField(max_length=60, null=True, blank=True)),
                ('h', models.CharField(max_length=60, null=True, blank=True)),
                ('description', models.CharField(max_length=125, null=True, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Label',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('x', models.CharField(max_length=60, null=True, blank=True)),
                ('y', models.CharField(max_length=60, null=True, blank=True)),
                ('w', models.CharField(max_length=60, null=True, blank=True)),
                ('h', models.CharField(max_length=60, null=True, blank=True)),
                ('image', models.ForeignKey(to='labelapp.Image')),
            ],
        ),
        migrations.CreateModel(
            name='LabelName',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label_name', models.CharField(max_length=30, null=True, blank=True)),
                ('description', models.CharField(max_length=125, null=True, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='label',
            name='label_name',
            field=models.ForeignKey(to='labelapp.LabelName'),
        ),
    ]
