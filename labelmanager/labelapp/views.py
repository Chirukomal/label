from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login
from django.conf import settings
from django.contrib.auth import logout
from django.shortcuts import render,render_to_response
from django.template import RequestContext, Context, Template
from django.http import HttpResponse, HttpRequest, \
                        HttpResponseRedirect, HttpResponseBadRequest, Http404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    return render(request,'home.html')

def lobby(request):
    """
    It will return lobby.
    it will return unique id of flash  session
    it will return data of login popup bonus
    depositlimiterror /withdrawal limit error will display on lobby .
    """
    message = ""
    

    if request.session.get('label_message'):
        message = request.session.get('label_message')
        del request.session['label_message']
    return render_to_response('welcome.html', {'data': message}, context_instance=RequestContext(request))

def userlog(request):
    #for user login
        
    response = {'status': "error"}
    user = str(request.POST.get('username'))
    pswd = str(request.POST.get('password'))
    uname = User.objects.filter(username = user)
    if uname.count()>0:
        user_valid = authenticate(username = user, password = pswd)
        if user_valid is not None:
            login(request,user_valid)
            request.session['label_message'] = 'Sucessfull'
            return HttpResponseRedirect(reverse('lobby'))
            #return render_to_response('welcome.html',context_instance=RequestContext(request))  
        else:
            response['message'] = 'username or password incorrect'
            response['status'] = 'error'
            return render_to_response('home.html',{'data': response},context_instance=RequestContext(request))

    else:
        response['message'] = 'please register'
        response['status'] = 'error'
        return render_to_response('home.html',{'data': response},context_instance=RequestContext(request))

@login_required
def userlogout(request):
    #for logout user
    
    response = {'status': "error"}
    logout(request)
    response['message'] = 'YOUR ARE LOGGED OUT'
    return render_to_response('home.html', {'data': response}, context_instance=RequestContext(request))