from django.db import models

# Create your models here.


class LabelName(models.Model):
    user = models.ForeignKey('auth.user')
    label_name = models.CharField(max_length = 30, null = True, blank = True)
    description = models.CharField(max_length = 125, null = True, blank = True)

class Image(models.Model):
    user = models.ForeignKey('auth.user')
    x = models.CharField(max_length = 60, null = True, blank = True)
    y = models.CharField(max_length = 60, null = True, blank = True)
    w = models.CharField(max_length = 60, null = True, blank = True) 
    h = models.CharField(max_length = 60, null = True, blank = True)
    uploadtime = models.DateTimeField(auto_now=True)
    description = models.CharField(max_length = 125, null = True, blank = True)
        

class Label(models.Model):
    label_name = models.ForeignKey(LabelName)
    image =  models.ForeignKey(Image)
    labeltime = models.DateTimeField(auto_now=True)
    x = models.CharField(max_length = 60, null = True, blank = True)
    y = models.CharField(max_length = 60, null = True, blank = True)
    w = models.CharField(max_length = 60, null = True, blank = True) 
    h = models.CharField(max_length = 60, null = True, blank = True)  
